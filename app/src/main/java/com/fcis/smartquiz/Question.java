package com.fcis.smartquiz;

/**
 * Created by Basil on 4/30/2016.
 */
public class Question {
    public int QuestionID ;
    public String QuestionTitle ;
    public String CorrectAnswer ;
    public String FirstOption ;
    public String SecondOption ;
    public String ThirdOption ;
}
