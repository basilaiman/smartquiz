package com.fcis.smartquiz;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
public class ScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);
        TextView tvScoreName = (TextView) findViewById(R.id.tvScoreName);
        TextView tvScoreS = (TextView) findViewById(R.id.tvScoreS);
        Intent passed = getIntent();
        String playerName = passed.getStringExtra("playername");
        String score = passed.getStringExtra("score");
        tvScoreS.setText(score);
        tvScoreName.setText(playerName);
        Button btnAgain = (Button) findViewById(R.id.btnAgain);
        btnAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),StartActivity.class);
                startActivity(i);
                finish();

            }
        });

    }
}
