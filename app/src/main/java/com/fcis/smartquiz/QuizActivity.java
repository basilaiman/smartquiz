package com.fcis.smartquiz;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import com.loopj.android.http.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.client.HttpClient;

public class QuizActivity extends AppCompatActivity {
    ArrayList<Question> QuestionList = new ArrayList<Question>();
    CountDownTimer timer ;
    TextView tvTime , tvScore ,tvQuestionTitle ;
    String level;
    Question currentQuestion;
    Button btn1,btn2,btn3,btn4;
    AsyncHttpClient httpClient;
    String playerName;
    int score =0;
    int answeredQuestion = 0;
    private  void setQuestionList (ArrayList<Question> newList){
        this.QuestionList = newList;
        LoadNewQuestion();
        StartGame();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        Intent passedIntent = getIntent();
         playerName = passedIntent.getStringExtra("playername");
        level = passedIntent.getStringExtra("level");
        ArrayList<String> catIDs = passedIntent.getStringArrayListExtra("ids");

        //Initilaize UI Componanat
        tvTime = (TextView) findViewById(R.id.tvTimer);
        tvScore =(TextView) findViewById(R.id.tvScore);
        tvQuestionTitle = (TextView)findViewById(R.id.tvQuestionTitle);
         btn1 = (Button) findViewById(R.id.button);
         btn2 = (Button) findViewById(R.id.button2);
         btn3 = (Button) findViewById(R.id.button3);
         btn4 = (Button) findViewById(R.id.button4);
        //-----------------------------
        // Get The question Online

        RequestParams params = new RequestParams();
        params.put("", catIDs);
         httpClient = new AsyncHttpClient();
        AsyncHttpResponseHandler handler = new AsyncHttpResponseHandler() {
            ArrayList<Question> tempQuestionList = new ArrayList<Question>();

            @Override

            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String JsonQuestions = "";
                for (int i = 0; i < responseBody.length; i++) {
                    JsonQuestions += (char) responseBody[i];
                }
                Log.i("Repsonse",JsonQuestions);
                try {
                    JSONArray arr = new JSONArray(JsonQuestions);
                    for (int i = 0; i < arr.length(); i++) {
                        JSONObject objTemp = arr.getJSONObject(i);
                        Question temp = new Question();
                        temp.QuestionID = objTemp.getInt("QuestionID");
                        temp.QuestionTitle = objTemp.getString("QuestionTitle");
                        temp.CorrectAnswer = objTemp.getString("CorrectAnswer");
                        temp.FirstOption = objTemp.getString("FirstOption");
                        temp.SecondOption = objTemp.getString("SecondOption");
                        temp.ThirdOption = objTemp.getString("ThirdOption");
                        tempQuestionList.add(temp);
                    }



                } catch (JSONException e) {

                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

            }

            @Override
            public void onFinish() {
                // Set the data in the global variable of the program
                QuizActivity.this.setQuestionList(tempQuestionList);
            }
        };
        httpClient.post("http://smartquiz.basilaiman.com/api/questions", params, handler);

        //---------------------------









    }

    //Start Game Method
    protected  void StartGame(){



        int levelTime =0; // The variable depends on the coming Level
        switch (level){
            case "EASY": levelTime = 90*1000;break;
            case "MEDIUM": levelTime= 60*1000; break;
            case "HARD": levelTime=30*1000; break;
        }
        // Set the Default Value
        updateTimer(levelTime/1000);

        timer = new CountDownTimer(levelTime,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                updateTimer((int)millisUntilFinished/1000);
            }

            @Override
            public void onFinish() {
                Intent i = new Intent(getApplicationContext(),ScoreActivity.class);
                 i.putExtra("playername", playerName);
                 i.putExtra("level", level);
                i.putExtra("score", tvScore.getText().toString());
                 startActivity(i);
                overridePendingTransition(R.anim.rotate,R.anim.rotate_out);
            }
        }.start();




    }
    // Method to update the time
    void updateTimer(int secondLeft){
        int minutes = secondLeft /60;
        int seconds = secondLeft - minutes*60;
        String secondString  = Integer.toString(seconds);
        if(secondString =="0")
            secondString="00";
        tvTime.setText(Integer.toString(minutes) + ":" + secondString);
    }

    //Answer Question Method
    public void AnswerQuestion(View view){
        Button clickedButton = (Button) view;
        String ClickedAnswer = clickedButton.getText().toString();
        if (ClickedAnswer.equals(currentQuestion.CorrectAnswer)){
            answeredQuestion++;
            score++;
        }
        else
            answeredQuestion++;
        tvScore.setText(score+"/"+answeredQuestion);
        LoadNewQuestion();



    }
    protected void LoadNewQuestion(){
        //Get the a random index of the question
        Random rnd = new Random();
        int randomIndex = rnd.nextInt(QuestionList.size());
        currentQuestion = QuestionList.get(randomIndex); // Get a random question
        tvQuestionTitle.setText(currentQuestion.QuestionTitle);
        String[] Answers = {currentQuestion.CorrectAnswer,currentQuestion.FirstOption,currentQuestion.SecondOption,currentQuestion.ThirdOption};

        Collections.shuffle(Arrays.asList(Answers));
        btn1.setText(Answers[0]);
        btn2.setText(Answers[1]);
        btn3.setText(Answers[2]);
        btn4.setText(Answers[3]);



    }


}
