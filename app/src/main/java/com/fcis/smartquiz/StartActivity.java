package com.fcis.smartquiz;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);


        final EditText txtUserName = (EditText)findViewById(R.id.txtUserName);
        final RadioGroup rbgLevel = (RadioGroup) findViewById(R.id.rdgLevel);

        Button btnGoToCategories = (Button) findViewById(R.id.btnGoToCategories);
        btnGoToCategories.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Start the Category Activity and Passing the Data
                Intent i = new Intent(getApplicationContext(),CategoriesActivity.class);

                RadioButton temp = (RadioButton) findViewById(rbgLevel.getCheckedRadioButtonId());
                    if(txtUserName.getText().toString().trim().length() != 0)
                    {
                         i.putExtra("level",temp.getText().toString().toUpperCase());
                         i.putExtra("playername",txtUserName.getText().toString());

                        startActivity(i);
                    }
                    else
                        Toast.makeText(getApplicationContext(),"Please enter a name",Toast.LENGTH_SHORT).show();

            }
        });
    }
}
