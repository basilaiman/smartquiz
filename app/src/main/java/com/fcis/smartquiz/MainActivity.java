package com.fcis.smartquiz;

import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnStartApp = (Button) findViewById(R.id.btnStartApp);
        ImageView logo = (ImageView) findViewById(R.id.imageView);
        Animation a = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate);
        logo.startAnimation(a);
        //Starting the Start Activity
        btnStartApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(),StartActivity.class);
                startActivity(i);

            }
        });
    }
}
