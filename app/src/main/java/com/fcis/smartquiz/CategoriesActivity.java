package com.fcis.smartquiz;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.Console;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class CategoriesActivity extends AppCompatActivity {
    //  The Download task class to retrieve the Data from the API
    public class DownloadTask extends AsyncTask<String,Void,ArrayList<Category>>{
        @Override
        protected ArrayList<Category> doInBackground(String... urls) {
            ArrayList<Category> res = new ArrayList<Category>();

            String result ="";
            URL url;
            HttpURLConnection urlConnection = null;

            try {
                // Get the JSON Data
                url = new URL(urls[0]);
                urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);
                int data = reader.read();
                while (data != -1){
                    char current = (char) data;
                    result += current;
                    data = reader.read();
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

            JSONArray arr = null;
            try {
                arr = new JSONArray(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            for(int i =0 ; i < arr.length();i++){
                try {
                    //Deserialize to Objects
                    JSONObject part = arr.getJSONObject(i);
                    Category temp = new Category();
                    temp.CatID = part.getInt("CatID");
                    temp.Title =  part.getString("Title");
                    temp.isChecked = false;
                    res.add(temp);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            return  res;
        }


    }

    // Creating the CustomClass Adapter
    public class CategoryAdapter extends ArrayAdapter{
        protected   ArrayList<Category> objects;
        public CategoryAdapter(Context context, int textViewResourceId, ArrayList<Category> objects) {
            super(context, textViewResourceId, objects);
            this.objects = objects;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
                View v = convertView;
                if (v == null) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = inflater.inflate(R.layout.categories_list_view,null);
            }
            final Category i = objects.get(position);

            if (i != null) {

                final CheckBox tvCats = (CheckBox) v.findViewById(R.id.tvCats);
                tvCats.setText(i.Title);
                tvCats.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        CheckBox temp = (CheckBox) buttonView;
                        i.isChecked =isChecked;




                    }
                });


            }

            // the view must be returned to our activity
            return v;

        }
        public ArrayList<Category> getData(){
            return  objects;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);
        DownloadTask task = new DownloadTask();
        task.execute("http://smartquiz.basilaiman.com/api/categories");
        //----------------------
        //Loading Data from previous Intent

        Intent passedIntent = getIntent();
         final String playerName = passedIntent.getStringExtra("playername");
         final String level = passedIntent.getStringExtra("level");

        //----------------------
        ArrayList<Category> categories = new ArrayList<Category>();
        try {
            categories = task.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        final CategoryAdapter categoryAdapter = new CategoryAdapter(this,R.layout.categories_list_view,categories);
        final ListView lv = (ListView) findViewById(R.id.listView);
        lv.setAdapter(categoryAdapter);
        Animation a = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);
        lv.startAnimation(a);

        Button btnStartGame = (Button) findViewById(R.id.btnStartGame);
        btnStartGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Category> finalData = categoryAdapter.getData();
                ArrayList<String> categoriesIDs = new ArrayList<String>();
                for(int i =0; i< finalData.size(); i++){
                    Category temp = finalData.get(i);
                    if(temp.isChecked)
                        categoriesIDs.add(Integer.toString(temp.CatID));
                }

                //Pass the Name and the level to the Quiz Activity
                Intent i = new Intent(getApplicationContext(), QuizActivity.class);
                i.putExtra("level",level);
                i.putExtra("playername",playerName);
                i.putExtra("ids",categoriesIDs);
                startActivity(i);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

            }
        });








    }

}
